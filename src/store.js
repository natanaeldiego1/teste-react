import { createStore } from 'redux';
import { reducers } from './redux/reducers/indexReducer';

const store = createStore(reducers);

export { store };