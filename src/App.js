import { ApolloProvider } from '@apollo/client';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import Routes from './routes';
import apolloClient from './apolloClient';
import { store } from './store';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <Provider store={store}>
      <ApolloProvider client={apolloClient}>
        <main>
          <div className="container">
            <BrowserRouter>
              <Routes />
              <ToastContainer autoClose={3000} position={toast.POSITION.TOP_RIGHT} />
            </BrowserRouter>
          </div>
        </main>
      </ApolloProvider>
    </Provider>
  );
}



export default App;
