import React from 'react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import App from '../App';
import Input from '../components/Inputs';

test('Tests for Pokemon screen', async () => {
  render(<App />)
  const { getByTestId: gerByInput } = render(<Input />);
  const fieldNode = await waitFor(
    () => gerByInput('form-field')
  )
  const inputSearch = 'Pikachu'
  fireEvent.change(
    fieldNode,
    {
      target: { value: inputSearch }
    })
  expect(fieldNode.value).toEqual(inputSearch)

  const cardPokemon = await waitFor(() =>
    expect('card-pokemon').toEqual('card-pokemon')
  )
})