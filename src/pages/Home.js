import { useState } from 'react';
import { useQuery, gql } from '@apollo/client';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import { actions } from '../redux/actions/actionPokemons';
import { selectors } from '../redux/selectors/selectorPokemons';

import NoResults from '../components/NoResults';
import SearchInput from '../components/Inputs';
import ListData from '../components/ListData';
import Header from './Header';
import './style.css';

const LIST_POKEMONS = gql`
  query GetExchangeRates($first: Int!) {
    pokemons(first: $first) {      
      id
      name
      image
      weaknesses
      resistant
      types
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      attacks {
        special {
          name
          type
        }
      }
      evolutions {
        id
        name
        image
      }
    }
  }
`;

function Home() {
  const { loading, error, data } = useQuery(LIST_POKEMONS, {
    variables: { first: 151 },
    pollInterval: 5000
  });
  const dispatch = useDispatch();
  const listPokemons = useSelector(selectors.getPokemons);
  const search = useSelector(selectors.searchPokemons);
  const isContent = useSelector(selectors.isContent);
  const [statusSearch, setStatusSearch] = useState(false);
  let history = useHistory();

  const handleChange = (event) => {
    let result;
    if (event.target.value.trim()) {
      setStatusSearch(true);
      result = filterItems(event.target.value);
      dispatch(actions.searchPokemons(result));
    } else {
      setStatusSearch(false);
      dispatch(actions.searchPokemons(listPokemons));
    }
  }

  const filterItems = query => {
    return listPokemons.pokemons.filter(function (el) {
      return el.name.toLowerCase().indexOf(query.toLowerCase()) > -1;
    });
  }

  const details = id => {
    dispatch(actions.idPokemon(id));
    history.push("/detalhes");
  }

  if (!loading) {
    if (!isContent) {
      dispatch(actions.addPokemons(data.pokemons));
      dispatch(actions.isContent(true));
    }
  }

  if (loading) {
    return (
      <div className="row loadign justify-content-center">
        <div className="spinner-grow text-success" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  };
  if (error) return <div className="row loadign justify-content-center h2">Error :(</div>;

  return (
    <>
      <Header />
      <div className="row">
        <div className="container">{SearchInput({ handleChange })}</div>
        {
          statusSearch ?
            search.pokemonsSearch.length > 0 ? search.pokemonsSearch.map((data) => ListData(data, details)) : NoResults()
            :
            listPokemons.pokemons.length > 0 ? listPokemons.pokemons.map((data) => ListData(data, details)) : NoResults()
        }
      </div>
    </>
  )

}

export default Home;
