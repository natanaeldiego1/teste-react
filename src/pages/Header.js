import { useHistory } from 'react-router-dom';
import ImageLogo from '../assets/pokemon.png';
function Header({ status }) {
  let history = useHistory();
  const backPage = () => {
    if (status) {
      history.push("/");
    }
  }

  return (
    <div className={`d-flex justify-content-sm-center ${status ? 'cursor-pointer' : ''}`} onClick={() => backPage()}>
      <img src={ImageLogo} className="logo-pokemon" title="Pokémon" alt="Pokémon" />
    </div>
  )
}

export default Header;