import { useEffect, useState, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";
import Icon from 'react-icons-kit';
import { ic_save } from 'react-icons-kit/md';
import { edit, close } from 'react-icons-kit/ionicons';
import { actions } from '../redux/actions/actionPokemons';
import { selectors } from '../redux/selectors/selectorPokemons';
import { toast } from 'react-toastify';
import Header from './Header';
import './style.css';

function Home() {
  const dispatch = useDispatch();
  const listPokemons = useSelector(selectors.getPokemons);
  const id = useSelector(selectors.idPokemon);
  const { idPokemon } = id;
  const [dataPokemon, setDataPokemon] = useState([]);
  let history = useHistory();
  const [namePokemon, setNamePokemon] = useState('');
  const clickModal = useRef(null);
  const [validateStatus, setValidateStatus] = useState(false);

  useEffect(() => {
    window.scrollTo(0, 0);

    if (idPokemon !== 0) {
      let result = listPokemons.pokemons.find((data) => data.id === idPokemon);
      if (result !== undefined && Object.keys(result).length > 0) {
        setNamePokemon(result.name);
        setDataPokemon(result);
      }
    }

  }, [idPokemon, listPokemons]);

  if (dataPokemon === undefined || Object.keys(dataPokemon).length <= 0) {
    setTimeout(() => {
      if (!idPokemon) {
        history.push("/");
      }
    }, 2000);
    return (
      <div className="row loadign justify-content-center">
        <div class="spinner-grow text-success" role="status">
          <span class="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  const evolutionPokemon = id => {
    dispatch(actions.idPokemon(id));
  }

  const handleChange = (event) => {
    if (event.target.value.trim()) {
      let name = event.target.value.replace(/\s/g, '');
      if (name.length <= 2) {
        setValidateStatus(true);
      } else {
        setValidateStatus(false);
      }
    } else {
      setValidateStatus(true);
    }
    setNamePokemon(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!validateStatus) {
      let resultData = listPokemons.pokemons.map((data) => data.id === idPokemon ? { ...data, name: namePokemon } : data);
      dispatch(actions.addPokemons(resultData));
      clickModal.current.click();
      toast.success("Nome do pokémon atualizado com sucesso!!!");
    }
  }

  const visibleModal = () => {
    clickModal.current.click();
  }

  const modal = () => {
    return (
      <>

        <button className="invisible" ref={clickModal} data-toggle="modal" data-target="#editPokemon" />
        <div className="modal fade" id="editPokemon" tabIndex="-1" role="dialog" aria-labelledby="editPokemonLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="editPokemonLabel">Atualizar pokémon</h5>
                <button className="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
              </div>
              <div className="modal-body">
                <form onSubmit={handleSubmit}>
                  <div class="form-group">
                    <label class="mb-1 h6">Nome</label>
                    <input class="form-control" type="text" value={namePokemon} onChange={handleChange} />
                    {validateStatus && (
                      <span className="text-danger">Este campo é obrigado, preencha com no mínimo 3 caracter</span>
                    )}
                  </div>
                  <div className="modal-footer">
                    <button className="btn btn-danger" type="button" data-dismiss="modal">
                      <Icon
                        className="icons-form-edit"
                        size={25}
                        icon={close}
                      />
                      Cancelar
                    </button>
                    <button className="btn btn-success" type="submit">
                      <Icon
                        className="icons-form-edit"
                        size={25}
                        icon={ic_save}
                      />
                      Salvar
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }

  return (
    <>
      {Header({ status: true })}
      <div className="container">

        <div className="row">
          <div className="col-lg-4 mb-2">
            <div className="card mb-4 car-detail-image-top">
              <div className="card-body text-center p-2">
                <img className="img-fluidX mb-5 img-detail" src={dataPokemon.image} alt={dataPokemon.name} title={dataPokemon.name} />
                <h2 className="text-primary cursor-pointer" onClick={() => visibleModal()}>
                  {dataPokemon.name}
                  <Icon
                    className="color-pincel"
                    size={30}
                    icon={edit}
                  />
                </h2>
              </div>
            </div>
          </div>

          <div className="col-lg-8">
            <div className="row">
              <div className="col-xl-12 col-md-6 mb-4">
                <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-primary h-100">
                  <div className="card-body">
                    <div className="d-flex align-items-center">
                      <div className="flex-grow-1">
                        <div className="small font-weight-bold text-primary mb-1 text-center"><h2>Peso e Altura</h2></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-6 col-md-6 mb-4">
                <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-primary h-100">
                  <div className="card-body">
                    <div className="d-flex align-items-center">
                      <div className="flex-grow-1">
                        <div className="small font-weight-bold text-primary mb-1">Peso máximo</div>
                        <div className="h5">{dataPokemon.weight.maximum}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-6 col-md-6 mb-4">
                <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-secondary h-100">
                  <div className="card-body">
                    <div className="d-flex align-items-center">
                      <div className="flex-grow-1">
                        <div className="small font-weight-bold text-secondary mb-1">Altura máxima</div>
                        <div className="h5">{dataPokemon.height.maximum}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-6 col-md-6 mb-4">
                <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-success h-100">
                  <div className="card-body">
                    <div className="d-flex align-items-center">
                      <div className="flex-grow-1">
                        <div className="small font-weight-bold text-success mb-1">Peso mínimo</div>
                        <div className="h5">{dataPokemon.weight.minimum}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xl-6 col-md-6 mb-4">
                <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-info h-100">
                  <div className="card-body">
                    <div className="d-flex align-items-center">
                      <div className="flex-grow-1">
                        <div className="small font-weight-bold text-info mb-1">Altura mínima</div>
                        <div className="h5">{dataPokemon.height.minimum}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-12 mb-4">

            <div className="row">
              <div className="col-xl-12 col-md-6 mb-4">
                <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-primary h-100">
                  <div className="card-body">
                    <div className="d-flex align-items-center">
                      <div className="flex-grow-1">
                        <div className="small font-weight-bold text-primary mb-1 text-center"><h2>Principais caracteríscas</h2></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {dataPokemon.types.map((value, i) => {
                let total = dataPokemon.types.length;
                let status = (total % 2 === 0);
                i += 1;
                let col = !status && total === i ? '12' : '6';
                return (
                  <div key={i} className={`col-xl-${col} col-md-6 mb-4`}>
                    <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-primary">
                      <div className="card-body">
                        <div className="d-flex align-items-center">
                          <div className="flex-grow-1">
                            <div className={`font-weight-bold 
                          ${i === 0 ? 'text-primary' : i === 1 ? 'text-secondary' : i === 2 ? 'text-success' : 'text-info'} mb-1 h6`}>
                              {value}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>

            <div className="row">
              <div className="col-xl-12 col-md-6 mb-4">
                <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-primary h-100">
                  <div className="card-body">
                    <div className="d-flex align-items-center">
                      <div className="flex-grow-1">
                        <div className="small font-weight-bold text-primary mb-1 text-center"><h2>Principais resistências</h2></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {dataPokemon.resistant.map((value, i) => {
                let total = dataPokemon.resistant.length;
                let status = (total % 2 === 0);
                i += 1;
                let col = !status && total === i ? '12' : '6';
                return (
                  <div key={i} className={`col-xl-${col} col-md-6 mb-4`}>
                    <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-primary">
                      <div className="card-body">
                        <div className="d-flex align-items-center">
                          <div className="flex-grow-1">
                            <div className={`font-weight-bold 
                          ${i === 0 ? 'text-primary' : i === 1 ? 'text-secondary' : i === 2 ? 'text-success' : 'text-info'} mb-1 h6`}>
                              {value}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>

            <div className="row">
              <div className="col-xl-12 col-md-6 mb-4">
                <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-primary h-100">
                  <div className="card-body">
                    <div className="d-flex align-items-center">
                      <div className="flex-grow-1">
                        <div className="small font-weight-bold text-primary mb-1 text-center"><h2>Principais fraquezas</h2></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {dataPokemon.weaknesses.map((value, i) => {
                let total = dataPokemon.weaknesses.length;
                let status = (total % 2 === 0);
                i += 1;
                let col = !status && total === i ? '12' : '6';
                return (
                  <div key={i} className={`col-xl-${col} col-md-6 mb-4`}>
                    <div className="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-primary">
                      <div className="card-body">
                        <div className="d-flex align-items-center">
                          <div className="flex-grow-1">
                            <div className={`font-weight-bold 
                          ${i === 0 ? 'text-primary' : i === 1 ? 'text-secondary' : i === 2 ? 'text-success' : 'text-info'} mb-1 h6`}>
                              {value}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>

            <div className="row">
              <div className="col-xl-12 col-md-6">
                <div className="card card-waves mb-4 cor-evolution">
                  <div className="card-body">
                    <div className="d-flex align-items-center">
                      <div className="flex-grow-1">
                        <div className="small font-weight-bold mb-1 text-center text-white"><h2>Evoluções</h2></div>
                      </div>
                    </div>
                  </div>
                  <div className="row p-5 d-flex justify-content-center">

                    {dataPokemon.evolutions !== null ? dataPokemon.evolutions.map((value, i) => (
                      <div key={i} className="card-body col-xl-3 col-md-4 d-flex justify-content-center">
                        <div className="row flex-column cursor-pointer" onClick={() => evolutionPokemon(value.id)}>
                          <img className="img-pokemons rounded-circle mb-2" src={value.image} alt={value.name} title={value.name} />
                          <div className="mb-4 text-white text-center font-weight-bold">{value.name}</div>
                        </div>
                      </div>
                    )) : (
                        <div className="card-body d-flex justify-content-center">
                          <div className="row flex-column">
                            <div className="small font-weight-bold mb-1 text-center text-white"><h2>Evolução final atingida </h2></div>
                            <img className="img-pokemons rounded-circle mb-2 align-self-center" src={dataPokemon.image} alt={dataPokemon.name} title={dataPokemon.name} />
                          </div>
                        </div>
                      )}
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
      {modal()}
    </>
  )

}

export default Home;
