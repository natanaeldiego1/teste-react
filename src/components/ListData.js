function ListData({ id, name, image, types }, details) {
  return (
    <div key={id} className="col-xl-3 col-md-6 mb-4">
      <div className="card-deck mb-4">
        <div data-testid="card-pokemon" className="card car-home-image cursor-pointer" onClick={() => details(id)}>
          <img src={image} className="card-img-top image-home shadow-lg" alt={name} title={name} height="236" />
          <div className="card-body">
            <h5 className="card-title text-center">{name}</h5>
          </div>
          <div className="card-footerp d-flex justify-content-sm-center mb-3">
            <small className="text-muted">
              {types.map((data, identKey) => (
                <button key={identKey} type="button" className={`btn m-1 btn-sm cursor-color ${data.toLowerCase()}`}>{data}</button>
              ))}
            </small>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ListData;