import Icon from 'react-icons-kit';
import { search } from 'react-icons-kit/fa/search';

function SearchInput({ handleChange }) {
  return (
    <form>
      <div className="col-auto">
        <div className="input-group mb-2">
          <input data-testid="form-field" type="text" className="form-control form-search" onChange={handleChange} placeholder="Buscar pokémons" />
          <div className="input-group-prepend">
            <div className="input-group-text input-search-right">
              <Icon
                className="color-pincel"
                size={30}
                icon={search}
              />
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}

export default SearchInput;