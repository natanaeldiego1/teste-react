function NoResults() {
  return (
    <div className="container">
      <div className="card card-waves mb-4 mt-5">
        <div className="card-body p-5">
          <div className="row align-items-center justify-content-between">
            <div className="col">
              <h2 className="text-primary text-center">Desculpe, não encontrei nenhum Pokémon!!!</h2>
            </div></div>
        </div>
      </div>
    </div>
  );
}

export default NoResults;