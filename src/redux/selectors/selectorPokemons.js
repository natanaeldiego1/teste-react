const getPokemons = state => state.pokemonsReducers.pokemons;
const searchPokemons = state => state.pokemonsReducers.searchPokemons;
const idPokemon = state => state.pokemonsReducers.idPokemon;
const isContent = state => state.pokemonsReducers.isContent;

const selectors = {
  getPokemons,
  searchPokemons,
  idPokemon,
  isContent
}

export { selectors };