import { createAction } from 'redux-actions';

import { types } from '../types/typePokemons';

const actions = {
  addPokemons: createAction(types.ADD_POKEMONS),
  searchPokemons: createAction(types.SEARCH_POKEMONS),
  updateNamePokemons: createAction(types.UPDATE_NAME_POKEMONS),
  idPokemon: createAction(types.ID_POKEMON),
  isContent: createAction(types.IS_CONTENT)
}

export { actions };