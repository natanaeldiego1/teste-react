import { combineReducers } from 'redux';

import { reducers as pokemonsReducers } from './reducerPokemons';

const reducers = combineReducers({
  pokemonsReducers
})

export { reducers };