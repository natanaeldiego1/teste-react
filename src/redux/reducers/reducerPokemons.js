import { combineReducers } from 'redux';
import { types } from '../types/typePokemons';

const INITIAL_STATE = {
  pokemons: [],
  pokemonsSearch: [],
  idPokemon: 0,
  isContent: false
}

const pokemons = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.ADD_POKEMONS:
      return { ...state, pokemons: action.payload }
    default:
      return state;
  }
};

const searchPokemons = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.SEARCH_POKEMONS:
      return { ...state, pokemonsSearch: action.payload }
    default:
      return state;
  }
};

const idPokemon = (state = INITIAL_STATE.idPokemon, action) => {
  switch (action.type) {
    case types.ID_POKEMON:
      return { ...state, idPokemon: action.payload }
    default:
      return state;
  }
};

const isContent = (state = INITIAL_STATE.idPokemon, action) => {
  switch (action.type) {
    case types.IS_CONTENT:
      return { ...state, isContent: action.payload }
    default:
      return state;
  }
};

const reducers = combineReducers({
  pokemons,
  searchPokemons,
  idPokemon,
  isContent
})

export { reducers };